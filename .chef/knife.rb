current_dir = File.dirname(__FILE__)


log_level                :info
log_location             STDOUT
node_name                "cli"
client_key               "#{current_dir}/access_certs/test-organisation-cli.pem"
chef_server_url          "https://ip-172-31-64-85.eu-west-1.compute.internal/organizations/test-organisation"
signing_key_filename     "#{current_dir}/keys/Offline_Installation_Test.pem"
aws_credential_file      "#{current_dir}/.aws/credentials"
aws_config_file		 "#{current_dir}/.aws/config"



