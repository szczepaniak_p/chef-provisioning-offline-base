require 'chef/provisioning/aws_driver'
   with_driver 'aws'
   environment  = 'offline-installation-test'

   with_chef_environment environment
   with_chef_server(
      "https://ip-172-31-64-85.eu-west-1.compute.internal/organizations/test-organisation",
      #with_chef_server Chef::Config[:chef_server_url],
      client_name: Chef::Config[:node_name],
      signing_key_filename: Chef::Config[:client_key]
)

  with_machine_options({
     convergence_options: {
        ssl_verify_mode: 'verify_none',
        never_forward_localhost: true,
        chef_version: "12.20.3"
   },
    ssh_username: "centos"
 })

 add_machine_options(
     bootstrap_options: {
     key_name: 'Offline_Installation_Test',
     instance_type: 'm3.medium',
     image_id: 'ami-ac524fca',
     subnet_id: 'subnet-057bc97d248e88cdd',
     security_group_ids: ['sg-09cc6c2c768a35e69']
  }
 )


### CONFIGURE YOUR CLUSTER BELOW ###

c8ycore_count = 3
flavour_for_c8ycore       = "c4.xlarge"

ontoplb_count = 1
flavour_for_ontoplb       = "m4.large"

ssagent_count = 1
flavour_for_ssagent       = "m4.large"
private_ips_for_ssagent   = ["172.31.18.250"]
ssagent_tags  = [
        ["cep","device-simulator","smartrule"],
]

mongodb_count = 3
flavour_for_mongodb       = "c4.2xlarge"
mongodb_cluster = [
        ["configreplset:config9:P","replicaset:rs01:P","replicaset:rs02:S","replicaset:rs03:A"],
        ["configreplset:config9:S","replicaset:rs01:A","replicaset:rs02:P","replicaset:rs03:S"],
        ["configreplset:config9:S","replicaset:rs01:S","replicaset:rs02:A","replicaset:rs03:P"]
]

kubernetes_master_count   = 3
flavour_for_masters       = "m4.large"

kubernetes_worker_count   = 3
flavour_for_workers       = "c4.xlarge"


### END OF CLUSTER CONFIGURATION ###


        1.upto(mongodb_count) do |i|
            machine "#{environment}_mongodb_#{i}" do
              action :destroy
            end
        end

        1.upto(c8ycore_count) do |i|
            machine "#{environment}_core_#{i}" do
              action :destroy
            end
        end

        1.upto(ontoplb_count) do |i|
            machine "#{environment}_lb_#{i}" do
              action :destroy
            end
        end

        1.upto(ssagent_count) do |i|
            machine "#{environment}_agent" do
              action :destroy
            end
        end

        1.upto(kubernetes_master_count) do |i|
            machine "#{environment}_master_#{i}" do
              action :destroy
            end
        end

        1.upto(kubernetes_worker_count) do |i|
            machine "#{environment}_worker_#{i}" do
              action :destroy
            end
        end

file "/tmp/.ps-#{environment}.steps" do
    action :delete
end

